//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ledstrip.h>
#include <delay.h>
#include "examples.h"
#include "rgb_test.h"
#include <stdlib.h>

static uint16_t var1, var2;
     
//==============================================================================
// �������, ����������� ��������� ��� ��������. ���������� ������ �� ���������� ����
//==============================================================================
int16_t rgb_test_tick(void)
{
  switch(var2)
  {
  // ������� ������� ����� � ������� fade-out
  case 0:
    ledstrip_set_3color(var1 / 2, 255, 0, 0);
    ledstrip_fade_out_all(RGB_TST_FADEOUT_STEP);
    break;
  // ������ ������� ����� � ������� fade-out
  case 1:
    ledstrip_set_3color(var1 / 2, 0, 255, 0);
    ledstrip_fade_out_all(RGB_TST_FADEOUT_STEP);
    break;
  // ����� ������� ����� � ������� fade-out
  case 2:
    ledstrip_set_3color(var1 / 2, 0, 0, 255);
    ledstrip_fade_out_all(RGB_TST_FADEOUT_STEP);
    break;
  // Ƹ���� ������� ����� � ������� fade-out
  case 3:
    ledstrip_set_3color(var1 / 2, 255, 255, 0);
    ledstrip_fade_out_all(RGB_TST_FADEOUT_STEP);
    break;
  // ���������� ������� ����� � ������� fade-out
  case 4:
    ledstrip_set_3color(var1 / 2, 255, 0, 255);
    ledstrip_fade_out_all(RGB_TST_FADEOUT_STEP);
    break;
  // ������� ������� ����� � ������� fade-out
  case 5:
    ledstrip_set_3color(var1 / 2, 0, 255, 255);
    ledstrip_fade_out_all(RGB_TST_FADEOUT_STEP);
    break;
  // ����� ������� ����� (������ R+G+B) � ������� fade-out
  case 6:
    ledstrip_set_3color(var1 / 2, 255, 255, 255);
    ledstrip_fade_out_all(RGB_TST_FADEOUT_STEP);
    break;
  }

  if (++var1 >= (LEDSTRIP_LEDNUM * 3))
  {
    var1 = 0;
    if (++var2 > 6)
      var2 = 0;
  }
  
  return RGB_TST_PERIOD;
}
//==============================================================================
