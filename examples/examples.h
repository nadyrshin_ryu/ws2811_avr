//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _EXAMPLES_H
#define _EXAMPLES_H

#include <types.h>

#include "rgb/rgb_stars.h"
#include "rgb/rgb_rainbow.h"
#include "rgb/rgb_test.h"


typedef struct
{
  uint8_t G;
  uint8_t R;
  uint8_t B;
} tRGB;


// ��������� � �������� ����� �������� �������� *source � �������� *desc
void StepChange(uint8_t *desc, uint8_t *source, uint8_t Step);
// ��������� � �������� ����� ����������� ���� pDesc � pSource
void StepChangeColor(uint8_t *pDesc, uint8_t *pSource, uint8_t Step);
// ��������� �������� ���� *pSource � ���� *pDesc
void CopyColor(uint8_t *pDesc, uint8_t *pSource);

#endif