//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <delay.h>
#include <ledstrip.h>
#include <examples.h>
#include <rgb.h>


static uint8_t Mode = 0;
static uint8_t fadeout_var = 0;


//==============================================================================
// ��������� ����������� ��������
//==============================================================================
void rgb_effects(void)
{
  int16_t delay = 1;
  
  if (Mode <= 7)
  {
    // �������� �¨���
    delay = rgb_stars_tick(Mode);
    ledstrip_update();
    delay_ms(delay);
  }
  else if (Mode == 8)
  {
    // ������ ����� ��� ����������
    if (++fadeout_var <= 64)
    {
      ledstrip_fade_out_all(4);
      ledstrip_update();
      delay_ms(25);
    }
    else
      Mode++;
  }
  else if (Mode == 9)
  {
    // �������� ������
    delay = rgb_rainbow_tick();
    ledstrip_update();
    delay_ms(delay);
  }
  else if (Mode == 10)
  {
    // �������� ���� �����������
    delay = rgb_test_tick();
    ledstrip_update();
    delay_ms(delay);
  }
  else
    Mode = 0;
}
//==============================================================================


//==============================================================================
// ��������� ����������� ����� �������� �� ���������
//==============================================================================
void rbg_nextmode(void)
{
  Mode++;
  fadeout_var = 0;
}
//==============================================================================
