//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ledstrip.h>
#include "examples.h"
#include "rgb_stars.h"
#include <stdlib.h>


static uint8_t StarPos[STARS_NUM];
static tRGB color[STARS_NUM];
static tRGB color2[STARS_NUM];
static uint16_t var[STARS_NUM] = {0, 3, 6, 9, 12, 15, 18, 21, 24, 27};


//==============================================================================
//
//==============================================================================
void rgb_star_getnewcolor(uint8_t submode, tRGB *color)
{
  uint8_t ColorMask = 0;
  color->R = color->G = color->B = 0;
    
  switch (submode)
  {
  case 0:
    // ��������� ���� �� 6 �������� (��� ������ � �������)
    ColorMask = (rand() % 6) + 1;
    color->R = ColorMask & (1 << 0) ? 255 : 0;
    color->G = ColorMask & (1 << 1) ? 255 : 0;
    color->B = ColorMask & (1 << 2) ? 255 : 0;
    break;
    // �������
  case 1:
    color->R = 255;
    break;
    // ������
  case 2:
    color->G = 255;
    break;
    // �����
  case 3:
    color->B = 255;
    break;
    // Ƹ����
  case 4:
    color->R = 255;
    color->G = 255;
    break;
    // ����������
  case 5:
    color->R = 255;
    color->B = 255;
    break;
    // �������
  case 6:
    color->G = 255;
    color->B = 255;
    break;
    // �����
  case 7:
    color->R = 255;
    color->G = 255;
    color->B = 255;
    break;
  }
}
//==============================================================================


//==============================================================================
//
//==============================================================================
void rgb_star(uint8_t submode, uint8_t star)
{
  var[star]++;
  // �������� ����� ������� ������
  if (var[star] >= 64)
  {
    var[star] = 0;
    
    // ������� ������
    StarPos[star] = rand() % LEDSTRIP_LEDNUM;
    // �������� ��������� ����
    rgb_star_getnewcolor(submode, &(color[star]));
    // ������ ������� ���� �����, � ������� ����� �������� ����� ����
    ledstrip_get_3color(StarPos[star], &(color2[star].R), &(color2[star].G), &(color2[star].B));
  }

  // ������ ��������� �����
  StepChangeColor((uint8_t *)&(color2[star]), (uint8_t *)&(color[star]), RGB_ST_ADD_STEP);
  // ����� ���� ����� ��������� � �����
  ledstrip_set_3color(StarPos[star], color2[star].R, color2[star].G, color2[star].B);
}
//==============================================================================


//==============================================================================
//
//==============================================================================
int16_t rgb_stars_tick(uint8_t submode)
{
  for (int i = 0; i < STARS_NUM; i++)
    rgb_star(submode, i);

  // ������ ����� ��� ����������
  ledstrip_fade_out_all(RGB_ST_DEC_STEP);
  
  return 50;
}
//==============================================================================
