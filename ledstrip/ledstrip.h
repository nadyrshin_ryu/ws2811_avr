//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _WS2812_H
#define _WS2812_H

#include "..\types.h"

// ��� ��� ����������� � ����� �����������
#define LEDSTRIP_Data_Port      PORTC
#define LEDSTRIP_Data_DDR       DDRC
#define LEDSTRIP_Data_Mask      ((1<<3) | (1<<5))

// ���������� ����������� � �������
#define LEDSTRIP_LEDNUM         50


extern uint8_t ledstrip_buff[];


void ledstrip_init(void);
void ledstrip_sendarray(uint8_t *data, uint16_t datalen);
void ledstrip_update(void);
void ledstrip_fade_in_all(uint16_t fade_step);
void ledstrip_fade_out_all(uint16_t fade_step);
void ledstrip_all_off(void);


// ��������� ������ �� ������ ���� �����
void ledstrip_get_3color(uint16_t LED_Num, uint8_t *R, uint8_t *G, uint8_t *B);
// ��������� ������������� RGB-�������� � LED � �������� LED_Num (��� ����������� �� WS2812)
void ledstrip_set_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B);
// ��������� ��������� � ����� ����� ��������� ����
void ledstrip_add_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B);
// ��������� ��������� ������� ��������� ������ ��� ����������� ����������
void ledstrip_dec_3color(uint16_t LED_Num, uint8_t R, uint8_t G, uint8_t B);



#endif