//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <ioavr.h>
#include <inavr.h>
#include <ledstrip.h>
#include "main.h"
#include "rgb.h"
#include <delay.h>


#define GET_ButtonState()     (MODE_Button_Pin & MODE_Button_Mask ? 1 : 0)


void main()
{
  uint8_t Button, ButtonOld;

  // ������������� ����� � ������� ��� ������������ �������
  MODE_Button_DDR &= ~MODE_Button_Mask;
  MODE_Button_Port |= MODE_Button_Mask;

  ledstrip_init();

  // �������������� ��������� ��������� �����
  srand(5);

  while (1)
  {
    rgb_effects();
    
    // ��������� ������� ������
    Button = GET_ButtonState();
    if (!Button && ButtonOld)   // ������ ������� ������
    {
      rbg_nextmode();
    }
    ButtonOld = Button;
  }
}
