//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _RGB_STARS_H
#define _RGB_STARS_H

#include "..\types.h"


//// ��������� ��������
// ���������� (�������) ������������ ����������� ����
#define STARS_NUM       10
// ��� ���������� ������
#define RGB_ST_ADD_STEP        4
// ��� �������� ������
#define RGB_ST_DEC_STEP        1

int16_t rgb_stars_tick(uint8_t submode);


#endif